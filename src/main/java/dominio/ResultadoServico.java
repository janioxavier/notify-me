package dominio;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resultado")
public enum ResultadoServico {
	SUCESSO,
	USUARIO_NAO_ENCONTRADO,
	SENHA_INCORRETA,
	CONTA_NAO_ENCONTRADA,
	USUARIO_ENCONTRADO, CATEGORIA_NAO_ENCONTRADA;
	
}
