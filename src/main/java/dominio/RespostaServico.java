package dominio;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resposta")
public class RespostaServico {
	private ResultadoServico resultadoServico;
	
	private Usuario resposta;
		
	public ResultadoServico getResultadoServico() {
		return resultadoServico;
	}
	
	public void setResultadoServico(ResultadoServico resultadoServico) {
		this.resultadoServico = resultadoServico;
	}
	
	public Usuario getResposta() {
		return resposta;
	}
	
	public void setResposta(Usuario resposta) {
		this.resposta = resposta;
	}
}
