package dominio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "conta")
@XmlRootElement(name = "conta")
public class Conta {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idConta;

	@Column(nullable = false)
	private String nome;
	private String descricao;
	@Temporal(value = TemporalType.DATE)
	private Date vencimento;
	
	@Column(nullable = false)
	private double valor;
	
	private String caminhoImagem;

	private String horaNotificacao;
	
	private boolean estaPaga;
	
	private boolean notificadaAntesVencimento;
	
	private boolean notificadaDepoisVencimento;
	
	private boolean notificadaNoVencimento;
	
	@Temporal(value = TemporalType.TIME)
	private Date ultimaModificao;
	
	@OneToOne
	private Categoria categoria;

	public Conta() {
		// Construtor default
	}

	public int getIdConta() {
		return idConta;
	}

	@XmlElement
	public void setIdConta(int id) {
		this.idConta = id;
	}

	public String getNome() {
		return nome;
	}

	@XmlElement
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	@XmlElement
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Date getVencimento() {
		return vencimento;
	}

	@XmlElement
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public double getValor() {
		return valor;
	}

	@XmlElement
	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getCaminhoImagem() {
		return caminhoImagem;
	}

	@XmlElement
	public void setCaminhoImagem(String caminhoImagem) {
		this.caminhoImagem = caminhoImagem;
	}

	public String getHoraNotificacao() {
		return horaNotificacao;
	}

	@XmlElement
	public void setHoraNotificacao(String horaNotificacao) {
		this.horaNotificacao = horaNotificacao;
	}

	public boolean isEstaPaga() {
		return estaPaga;
	}

	public void setEstaPaga(boolean estaPaga) {
		this.estaPaga = estaPaga;
	}

	public boolean isNotificadaAntesVencimento() {
		return notificadaAntesVencimento;
	}

	public void setNotificadaAntesVencimento(boolean notificadaAntesVencimento) {
		this.notificadaAntesVencimento = notificadaAntesVencimento;
	}

	public boolean isNotificadaDepoisVencimento() {
		return notificadaDepoisVencimento;
	}

	public void setNotificadaDepoisVencimento(boolean notificadaDepoisVencimento) {
		this.notificadaDepoisVencimento = notificadaDepoisVencimento;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isNotificadaNoVencimento() {
		return notificadaNoVencimento;
	}

	public void setNotificadaNoVencimento(boolean notificadaNoVencimento) {
		this.notificadaNoVencimento = notificadaNoVencimento;
	}

	public Date getUltimaModificao() {
		return ultimaModificao;
	}

	public void setUltimaModificao(Date ultimaModificao) {
		this.ultimaModificao = ultimaModificao;
	}

	
	
	@Override
	public String toString() {
		return "Conta [idConta=" + idConta + ", nome=" + nome + ", descricao=" + descricao + ", vencimento="
				+ vencimento + ", valor=" + valor + ", caminhoImagem=" + caminhoImagem + ", horaNotificacao="
				+ horaNotificacao + ", estaPaga=" + estaPaga + ", notificadaAntesVencimento="
				+ notificadaAntesVencimento + ", notificadaDepoisVencimento=" + notificadaDepoisVencimento
				+ ", notificadaNoVencimento=" + notificadaNoVencimento + ", ultimaModificao=" + ultimaModificao
				+ ", categoria=" + categoria + "]";
	}

	@Override
	public boolean equals(Object obj) {
		Conta conta = (Conta) obj;
		// TODO Auto-generated method stub
		return this.getIdConta() == conta.getIdConta();
	}
}
