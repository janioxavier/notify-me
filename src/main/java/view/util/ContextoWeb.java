package view.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

public class ContextoWeb {
	
	public static RequestContext getRequestContext() {
		return RequestContext.getCurrentInstance();		
	}
	
	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public static HttpSession getSession() {
		HttpSession httpSession = null;		
		FacesContext fc = getFacesContext();
		ExternalContext ec = null;
		
		if (fc != null)
			ec = getFacesContext().getExternalContext();
		if (ec != null)
			httpSession = (HttpSession) ec.getSession(false);
		return httpSession;
	}
	
	public static void setAttribute(String nome, Object atributo) {
		HttpSession httpSession = getSession();
		if (httpSession != null) {		
			getSession().setAttribute(nome, atributo);
		}
		
	}
	
	public static Object getAttribute(String nome) {
		HttpSession httpSession = getSession();
		if (httpSession != null)
			return getSession().getAttribute(nome);
		return null;
	}
	
	public static void terminarSessao() {
		getFacesContext().getExternalContext().invalidateSession();
	}
}
