package view.util;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import dominio.Conta;

@XmlRootElement(name = "contas")
public class ContaListWrapper {
	private List<Conta> contas;
	
	@XmlElement(name = "conta")
	public List<Conta> getContas() {
		return contas;
	}
	
	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}
}
