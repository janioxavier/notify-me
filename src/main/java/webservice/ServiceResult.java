package webservice;

public enum ServiceResult {
	SUCESS("<result>success</result>"),
	FAILURE("<result>failure</result>"),
	USER_NOT_FOUND("<result>user not founded</result>"),
	ACCOUNT_NOT_FOUND("<result>account not founded</result>");
	
	private String result;
	
	private ServiceResult(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public String toString() {
		return result;
	}
	
}
