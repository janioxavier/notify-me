package webservice;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dominio.Conta;
import dominio.RespostaServico;
import dominio.Usuario;
import negocio.UsuarioService;

@Path("/UsuarioService")
public class UsuarioRest {

	@EJB
	private UsuarioService usuarioService;

	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico autenticarUsuario(@MatrixParam("login") String login, @MatrixParam("senha") String senha) {
		return usuarioService.doLogin(login, senha);
	}
	
	@PUT
	@Path("/user")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico addContaUsuario(Usuario usuario) {
		/*Usuario usuario = usuarioDAO.buscarUsuario(idUsuario);
		Conta contaAdicionada = new Conta();
		if (usuario != null) {
			contaDAO.salvar(conta);
			contaAdicionada = conta;
			usuario.getContas().add(conta);
			usuarioDAO.atualizarConta(usuario);
		} else {
		}*/
		return usuarioService.signup(usuario);
	}
	

	@PUT
	@Path("/user/{userId}/conta")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico addConta(@PathParam("userId") int idUsuario, Conta conta) {
		/*Usuario usuario = usuarioDAO.buscarUsuario(idUsuario);
		Conta contaAdicionada = new Conta();
		if (usuario != null) {
			contaDAO.salvar(conta);
			contaAdicionada = conta;
			usuario.getContas().add(conta);
			usuarioDAO.atualizarConta(usuario);
		} else {
		}*/
		return usuarioService.adicionarConta(idUsuario, conta);
	}

	@POST
	@Path("/user/{idUser}/conta")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico editarConta(@PathParam("idUser") int idUser, Conta contaEditada) {
		return usuarioService.editarConta(idUser, contaEditada);
	}

	@DELETE
	@Path("/user/{idUser}/conta/{idConta}")
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico deletarConta(@PathParam("idUser") int idUser, @PathParam("idConta") int idConta) {		
		return usuarioService.deletarConta(idUser, idConta);
	}
	
	@GET
	@Path("/user/{idUser}")	
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico getUsuario(@PathParam("idUser") int idUser) {
		return usuarioService.buscarUsuario(idUser);
	}
	
	@POST
	@Path("/user/{idUser}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public RespostaServico atualizarUsuario(@PathParam("idUser") int idUser, Usuario usuario) {
		return usuarioService.atualizarUsuario(usuario);
	}
}
