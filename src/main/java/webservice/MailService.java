package webservice;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailService {
	private String email = "projectnotifyme@gmail.com";
	private String senha = "notifymefrs";

	private Properties props = new Properties();

	private Session sessao;

	public MailService(String email, String senha) {
		this.email = email;
		this.senha = senha;
		iniciarServidorEmail();
	}

	private void iniciarServidorEmail() {	
		
		 props.put("mail.smtp.host", "smtp.gmail.com");
         props.put("mail.smtp.socketFactory.port", "465");
         props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
         props.put("mail.smtp.auth", "true");
         props.put("mail.smtp.port", "465");


		sessao = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, senha);
			}
		});
		sessao.setDebug(true);
	}

	public void enviarMensagem(String para, String assunto, String conteudo) throws MessagingException {
		Message mensagem = new MimeMessage(sessao);

		mensagem.setFrom(new InternetAddress(email));
		mensagem.setRecipients(Message.RecipientType.TO, InternetAddress.parse(para));
		mensagem.setSubject(assunto);
		mensagem.setText(conteudo);

		Transport.send(mensagem);
		
		System.out.println("mensagem enviada para " + para);

	}
	
	public static void main(String...args) throws MessagingException {
		String email = "projectnotifyme@gmail.com";
		String senha = "notifymefrs";
		MailService emailServico = new MailService(email, senha);
		emailServico.enviarMensagem("janiofx@gmail.com", "Conta enviada do notifyme", "teste");
		System.out.println("enviada");
		
	}

}

