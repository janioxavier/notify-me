package webservice; 

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.client.ClientProtocolException;

import dao.ContaDao;
import dominio.Conta;
import dominio.ResultadoServico;
import util.ClienteWeb;
import view.util.ContaListWrapper;

@Stateless
@Path("/ContaService")
public class ContaRest {
/*
	@Inject
	private ContaDAO dao;

	public ContaRest() {

	}
	
	@GET
	@Path("/user/{userId}")
	@Produces(MediaType.APPLICATION_XML)
	public ContaListWrapper getContas(@PathParam("userId") int idUser) {
		ContaListWrapper wrapper = new ContaListWrapper();
		//wrapper.setContas(dao.buscarContaUsuario(idUser));
		return wrapper;
	}
	
	@PUT
	@Path("/contas")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public String addConta(Conta conta) {
		ResultadoServico sr = null;
		try {
			dao.salvar(conta);
			sr = ResultadoServico.SUCESS;
		} catch (Exception e) {
			sr = ResultadoServico.FAILURE;
		}
		return null;
	}

	@DELETE
	@Path("/contas/{contaId}")
	@Produces(MediaType.APPLICATION_XML)
	public String deleteConta(@PathParam("contaId") int id) {
		boolean result = dao.remover (id);
		ResultadoServico sr = null;
		if (result) {
			sr = ResultadoServico.SUCESS;
		} else {
			sr = ResultadoServico.FAILURE;
		}
		return sr.getResult();
	}

	public static void main(String... args) {
		System.out.println("hi");

		String REST_SERVICE_URL = "http://localhost:8080/NotifyMe/api/ContaService";

		ClienteWeb client = new ClienteWeb();
		
		try {
			Conta conta = new Conta();
			conta.setNome("Conta de Teste");
			Date date = Date.from(Instant.now());			
			conta.setVencimento(date);
			
			StringWriter writer = new StringWriter();
			JAXBContext context = JAXBContext.newInstance(ContaListWrapper.class);
			Marshaller m = context.createMarshaller();

			m.marshal(conta, writer);
			
			System.out.println(writer.toString());					
			client.doPut(REST_SERVICE_URL + "/contas", writer.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		// buscar um arquivo xml do servi�o web abrindo uma conexao com o url do
		// tipo GET

		// HttpContext h = new HttpContext();
		// ClientContextConfigurer client = new ClientContextConfigurer(h);

		// carrega uma lista de contas de um arquivo xml
		try {
			
			JAXBContext context = JAXBContext.newInstance(ContaListWrapper.class);
			Unmarshaller um = context.createUnmarshaller();
			

			ContaListWrapper wrapper = (ContaListWrapper) um.unmarshal(client.doGet(REST_SERVICE_URL + "/user/1"));
			
			// exibe as contas
			System.out.println(wrapper.getContas());

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

}
