package webservice.login;

import org.json.JSONObject;

public class UsuarioFacebook {
	private Long id;
	private String firstName;
	private Integer timezone;
	private String email;
	private Boolean verified;
	private String middleName;
	private String gender;
	private String lastName;
	private String link;
	private String locale;
	private String name;
	private String updatedTime;
 
	public UsuarioFacebook(JSONObject jsonUsuario){
 
		id = jsonUsuario.getLong("id");
		name = jsonUsuario.getString("name");
		/*
		firstName = jsonUsuario.getString("first_name");
		timezone = jsonUsuario.getInt("timezone");
		email = jsonUsuario.getString("email");
		verified = jsonUsuario.getBoolean("verified");
		middleName = jsonUsuario.getString("middle_name");
		gender = jsonUsuario.getString("gender");
		lastName = jsonUsuario.getString("last_name");
		link = jsonUsuario.getString("link");
		locale = jsonUsuario.getString("locale");
		name = jsonUsuario.getString("name");
		updatedTime = jsonUsuario.getString("updated_time");*/
 
	}
 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getTimezone() {
		return timezone;
	}

	public void setTimezone(Integer timezone) {
		this.timezone = timezone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	@Override
	public String toString() {
		return "UsuarioFacebook [id=" + id + ", firstName=" + firstName
				+ ", timezone=" + timezone + ", email=" + email + ", verified="
				+ verified + ", middleName=" + middleName + ", gender="
				+ gender + ", lastName=" + lastName + ", link=" + link
				+ ", locale=" + locale + ", name=" + name + ", updatedTime="
				+ updatedTime + "]";
	}
 
}