package webservice;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import dominio.Usuario;
import negocio.UsuarioService;
import webservice.login.FacebookAuthHelper;
import webservice.login.UsuarioFacebook;

/*
@Path("/facebook")
public class FacebookLoginRest {
	@EJB
	private UsuarioService usuarioService;
	
	@GET
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void autenticarUsuario(@QueryParam("code") String code) throws MalformedURLException, IOException {
		UsuarioFacebook usuarioFacebook = new FacebookAuthHelper().obterUsuarioFacebook(code);
		Usuario usuario = getUsuarioApplication(usuarioFacebook);		
		usuarioService.signup(usuario);		
	}
	
	/**
	 * converter um usuario do facebook para um usuario da aplica��o
	 * @param usuarioFace
	 * @return usuario da aplica��o
	 *
	private Usuario getUsuarioApplication(UsuarioFacebook usuarioFace) {
		Usuario usuario = new Usuario();		
		usuario.setFirstName(usuarioFace.getFirstName());		
		usuario.setLogin(usuarioFace.getName()+usuarioFace.getId());
		usuario.setSenha(""+usuarioFace.getId());
		return usuario;
	}
	
	
}
*/