package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ContaDao;
import dominio.Conta;

@Stateless
public class ContaEJB {
	
	@Inject
	private ContaDao contaDAO;
	
	public ContaEJB() {		
	}
	
	public void cadastrarConta(Conta conta) {
		//conta.getUsuario().getContas().add(conta);
		contaDAO.salvar(conta);
	}

	/**
	 * atualiza as contas de dado usuario
	 * @param conta determinada conta
	 */
	public void atualizarConta(Conta conta) {
		contaDAO.atualizar(conta);
	}

	public void deletarConta(Conta conta) {
		contaDAO.remover(conta);
	}

	public Conta buscarConta(int idConta) {
		
		return contaDAO.buscarConta(idConta);
	}
}
