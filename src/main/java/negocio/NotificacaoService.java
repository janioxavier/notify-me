package negocio;

import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.mail.MessagingException;

import dao.UsuarioDao;
import dominio.Conta;
import dominio.Usuario;
import util.DataHora;
import webservice.MailService;

@Singleton
@Startup
public class NotificacaoService {
	private final long TEMPO_NOTIFICACAO = 100000;

	private final int DIA_ANTES_VENCIMENTO = 2;

	private final int DIA_DEPOIS_VENCIMENTO = 2;

	private final String EMAIL = "projectnotifyme@gmail.com";
	private final String SENHA = "notifymefrs";

	private final MailService servicoEmail = new MailService(EMAIL, SENHA);

	private final String ASSUNTO = "[NotifyMe] Notifica��o de Conta";

	public NotificacaoService() {

	}

	@Inject
	public NotificacaoService(UsuarioDao usuarioDAO) {
		Runnable r = () -> {
			while (true) {
				List<Usuario> usuarios = usuarioDAO.getUsuarios();

				usuarios.forEach((u) -> {
					if (u.getEmail1() != null) {
						List<Conta> contas = u.getContas();
						contas.forEach((c) -> notificarConta(u, c));
					}
				});

				try {
					Thread.sleep(TEMPO_NOTIFICACAO);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		new Thread(r).start();
	}

	private void notificarConta(Usuario usuario, Conta conta) {
		// notificar se a conta deve ser notificada antes do vencimento
		if (!conta.isNotificadaAntesVencimento() && usuario.isAntesVencimento()) {
			notificarVencimentoConfigurado(conta, usuario.getEmail1(), "vence no dia",
					DataHora.diminuirDia(conta.getVencimento(), DIA_ANTES_VENCIMENTO));
			conta.setNotificadaAntesVencimento(true);
		} else

		// notificar se a conta deve ser notificada no vencimento
		if (!conta.isNotificadaNoVencimento()) {
			notificarVencimentoConfigurado(conta, usuario.getEmail1(), "vence hoje", DataHora.getDataHoraAtual());
			conta.setNotificadaNoVencimento(true);
		} else

		// notificar se a conta deve ser notificada depois do vencimento
		if (!conta.isNotificadaDepoisVencimento() && usuario.isDepoisVencimento()) {
			notificarVencimentoConfigurado(conta, usuario.getEmail1(), "venceu no dia",
					DataHora.addDia(conta.getVencimento(), DIA_DEPOIS_VENCIMENTO));
			conta.setNotificadaDepoisVencimento(true);
		}
	}

	private void notificarVencimentoConfigurado(Conta conta, String email, String mensagem, Date dataHoraConfigurada) {
		Date dataHoraAtual = DataHora.getDataHoraAtual();
		// TODO verifica se a data esta formatada como data apenas
		// verifica se � o dia do vencimento
		if (DataHora.ehMesmoDiaHoraMinuto(dataHoraAtual, dataHoraConfigurada)) {
			try {
				enviarMensagemFormatada(email,
						ASSUNTO + " " + mensagem + " " + DataHora.formatarData(dataHoraConfigurada), conta);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private void enviarMensagemFormatada(String para, String assunto, Conta conta) throws MessagingException {
		String conteudo = gerarConteudo(conta);

		servicoEmail.enviarMensagem(para, assunto, conteudo);
	}

	private String gerarConteudo(Conta conta) {
		String conteudo = "Nome: " + conta.getNome() + "\n";

		if (conta.getDescricao() != null) {
			conteudo = "descri��o: " + conta.getDescricao() + "\n";
		}

		conteudo += "valor: " + String.format("%.2f", conta.getValor()) + "\n";

		return conteudo;
	}

	/*
	 * private void notificarDepoisVencimento(Conta conta, String email, Date
	 * dataNotificacaoPosterior) { Date dataAtual = DataHora.getDataHoraAtual();
	 * 
	 * if (dataAtual.after(dataNotificacaoPosterior)) { try {
	 * enviarMensagemFormatada(email, ASSUNTO + " venceu no dia " +
	 * DataHora.formatarData(conta.getVencimento()), conta);
	 * conta.setNotificadaDepoisVencimento(true); } catch (MessagingException e)
	 * { // TODO Auto-generated catch block e.printStackTrace(); }
	 * 
	 * } }
	 */

	/*
	 * private void notificarAntesVencimento(Conta conta, String email, Date
	 * dataNotificacaoAnterior) { // decrementa dois dias da data e hora do
	 * vencimento da conta Date dataAtual = Date.from(Instant.now());
	 * 
	 * if (dataAtual.after(dataNotificacaoAnterior)) { try {
	 * enviarMensagemFormatada(email, ASSUNTO + " vai vencer � " +
	 * DataHora.formatarData(conta.getVencimento()), conta);
	 * conta.setNotificadaAntesVencimento(true); } catch (MessagingException e)
	 * { // TODO Auto-generated catch block e.printStackTrace(); } } }
	 */

}
