package negocio;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.CategoriaDao;
import dao.UsuarioDao;
import dominio.Categoria;
import dominio.Conta;
import dominio.RespostaServico;
import dominio.ResultadoServico;
import dominio.Usuario;

@Stateless
public class UsuarioService {
	@Inject
	private UsuarioDao usuarioDAO;

	@Inject
	private CategoriaDao categoriaDAO;

	@EJB
	private ContaEJB contaEJB;

	private RespostaServico resposta;

	private static final ResultadoServico SUCESSO = ResultadoServico.SUCESSO;

	private static final ResultadoServico SENHA_INCORRETA = ResultadoServico.SENHA_INCORRETA;

	private static final ResultadoServico USUARIO_NAO_ENCONTRADO = ResultadoServico.USUARIO_NAO_ENCONTRADO;

	private static final ResultadoServico USUARIO_ENCONTRADO = ResultadoServico.USUARIO_ENCONTRADO;

	private static final ResultadoServico CONTA_NAO_ENCONTRADA = ResultadoServico.CONTA_NAO_ENCONTRADA;

	private static final ResultadoServico CATEGORIA_NAO_ENCONTRADA = ResultadoServico.CATEGORIA_NAO_ENCONTRADA;

	public UsuarioService() {
		resposta = new RespostaServico();
	}

	/**
	 * 
	 * @param login
	 *            login do usuario
	 * @param senha
	 *            senha do usuario
	 * @return 1 se o login e senha est�o incorretos, 0 se a senha estiver
	 *         incorreta, -1 se o usu�rio nao estiver cadastrado.
	 */
	public RespostaServico doLogin(String login, String senha) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(login);
		if (usuarioNoBanco != null) {
			if (usuarioNoBanco.getLogin().equals(login) && usuarioNoBanco.getSenha().equals(senha)) {
				// usuario e senha corretos
				resposta.setResultadoServico(SUCESSO);
			} else {
				// usuario ou senha incorretos
				resposta.setResultadoServico(SENHA_INCORRETA);
				usuarioNoBanco = null;
			}
		} else {
			// usuario nao encontrado
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	/**
	 * Busca um usuario cadastrado no sistema
	 * 
	 * @param login
	 *            login de um usuario
	 * @return um usuario cadastrado no sistema ou null caso o usuario nao for
	 *         encontrado
	 */
	public RespostaServico buscarUsuario(String login) {
		resposta.setResposta(usuarioDAO.buscarUsuario(login));
		if (resposta.getResposta() != null) {
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		return resposta;
	}

	/**
	 * 
	 * @param usuario
	 *            usuario com dados de usuario
	 * @return true se o cadastro ocorreu com sucesso, false caso o login ja
	 *         estiver em uso
	 */
	public RespostaServico signup(Usuario usuario) {
		resposta = buscarUsuario(usuario.getLogin());
		if (resposta.getResposta() == null) {
			usuarioDAO.salvar(usuario);
			resposta.setResposta(usuario);
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_ENCONTRADO);
		}
		return resposta;
	}

	/**
	 * 
	 * @param confirmPassword
	 *            confirmacao do password
	 * @return true se o usuario foi deletado, false caso contrario.
	 */
	public RespostaServico deletarUsuario(String login, String confirmPassword) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(login);
		if (usuarioNoBanco != null) {
			if (usuarioNoBanco.getSenha().equals(confirmPassword)) {
				usuarioDAO.deletar(usuarioNoBanco);
				resposta.setResultadoServico(ResultadoServico.SUCESSO);
			} else {
				resposta.setResultadoServico(ResultadoServico.SENHA_INCORRETA);
			}
		} else {
			resposta.setResultadoServico(ResultadoServico.USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	/**
	 * @param novoLogin
	 *            novo login do usuario
	 * @param confirmPassword
	 *            password de confirmacao
	 * @return 1 se o login foi alterado com sucesso, 0 caso o confirmPassword
	 *         estiver incorreto -1 caso o login j� estiver sendo utilizado.
	 */
	public RespostaServico atualizarLogin(String loginAtual, String novoLogin, String confirmPassword) {
		// Usuario usuarioEncontado =
		Usuario usuarioNoBanco = null;
		// verifica se existe um usuario no banco cadastrado com o novo login
		if (!usuarioDAO.temUsuario(novoLogin)) {
			// busca o usuario com o login atual no banco de dados
			usuarioNoBanco = usuarioDAO.buscarUsuario(loginAtual);
			if (usuarioNoBanco.getSenha().equals(confirmPassword)) {
				usuarioNoBanco.setLogin(novoLogin);
				// usuarioDAO.atualizar(usuario);
				resposta.setResultadoServico(SUCESSO);
			} else {
				resposta.setResultadoServico(SENHA_INCORRETA);
			}
		} else {
			// tem um usuario cadastrado com esse login
			resposta.setResultadoServico(USUARIO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	/**
	 * @param antigoPassword
	 *            antigo password
	 * @param novoPassword
	 *            novo password do usuario
	 * @param confirmPassword
	 *            password de confirmacao
	 * @return 1 se o password foi alterado com sucesso, 0 caso o novoPassword
	 *         estiver incorreto -1 caso a novoPassword estiver diferente da
	 *         confirmPassword.
	 */
	public RespostaServico atualizarSenha(String login, String antigoPassword, String novoPassword) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(login);
		// verifica se o usuario esta no banco
		if (usuarioNoBanco != null) {
			// compara a antiga senha com a senha do usuario no banco
			if (antigoPassword.equals(usuarioNoBanco.getSenha())) {
				// TODO verificar se a senha no banco de dados � alterada
				usuarioNoBanco.setSenha(novoPassword);
				resposta.setResposta(usuarioNoBanco);
				resposta.setResultadoServico(SUCESSO);
				/*
				 * if (novoPassword.equals(confirmPassword)) { // sucesso senha
				 * alterada com sucesso usuarioNoBanco.setSenha(novoPassword);
				 * // atualizarUsuario(usuario);
				 * //usuarioDAO.atualizar(usuario);
				 * resposta.setResposta(usuarioNoBanco); } else { // novo
				 * password e antigo não confere
				 * resposta.setResultadoServico(ResultadoServico.); }
				 */
			} else {
				// Senha incorreta
				resposta.setResultadoServico(SENHA_INCORRETA);
			}
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	/**
	 * 
	 * @param usuario
	 *            usuario do sistema.
	 * @return true se o perfil foi alterado com sucesso, false caso algum o
	 *         primeiroNome e sobrenome do usuario estiver vazio.
	 */
	public RespostaServico atualizarProfile(String login, String nome, String sobrenome) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(login);
		if (usuarioNoBanco != null) {
			usuarioNoBanco.setFirstName(nome);
			usuarioNoBanco.setLastName(sobrenome);
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico buscarUsuario(int idUser) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUser);
		if (usuarioNoBanco != null) {
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico adicionarConta(int idUsuario, Conta conta) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		if (usuarioNoBanco != null) {
			Categoria categoria = conta.getCategoria();
			// verifica se o usuario possui determinada categoria
			if (categoria != null) {
				final String nomeCategoria = categoria.getNome();
				Optional<Categoria> categoriaOP = usuarioNoBanco.getCategorias().stream()
						.filter((x) -> x.getNome().equals(nomeCategoria)).findFirst();
				if (categoriaOP.isPresent()) {
					conta.setCategoria(categoriaOP.get());
				} else {
					categoriaDAO.salvar(categoria);
					// persiste a categoria na relacao usuario categoria
					usuarioNoBanco.getCategorias().add(categoria);
				}
			}
			contaEJB.cadastrarConta(conta);
			usuarioNoBanco.getContas().add(conta);
			resposta.setResultadoServico(SUCESSO);
			// usuarioDAO.atualizar(usuario);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico editarConta(int idUsuario, Conta contaEditada) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		return editarConta(usuarioNoBanco, contaEditada);
	}

	public RespostaServico editarConta(Usuario usuarioNoBanco, Conta contaEditada) {
		if (usuarioNoBanco != null) {
			// verifica se o usuario possui a conta
			int index = usuarioNoBanco.getContas().indexOf(contaEditada);
			if (index != -1) {
				contaEJB.atualizarConta(contaEditada);
				usuarioNoBanco.getContas().set(index, contaEditada);
				resposta.setResultadoServico(SUCESSO);
			} else {
				resposta.setResultadoServico(CONTA_NAO_ENCONTRADA);
			}
			/*
			 * List<Conta> contasUsuario = usuarioNoBanco.getContas(); Conta
			 * contaAntiga = contaEJB.buscarConta(idConta); if (contaAntiga !=
			 * null) { // edita a conta se o usuario a tive for (Conta c :
			 * contasUsuario) { if (c.getIdConta() == contaEditada.getIdConta())
			 * { contaEJB.atualizarConta(contaEditada);
			 * resposta.setResposta(usuarioNoBanco); break; } } } else {
			 * resposta.setResultadoServico(CONTA_NAO_ENCONTRADA); }
			 */

		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico deletarConta(int idUsuario, int idConta) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		Conta conta = contaEJB.buscarConta(idConta);
		return deletarConta(usuarioNoBanco, conta);
	}

	public RespostaServico deletarConta(Usuario usuarioNoBanco, Conta conta) {
		if (usuarioNoBanco != null) {
			// verifica se o usuario possui a conta
			if (usuarioNoBanco.getContas().remove(conta)) {
				contaEJB.deletarConta(conta);
				resposta.setResultadoServico(SUCESSO);
			} else {
				resposta.setResultadoServico(CONTA_NAO_ENCONTRADA);
			}
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico atualizarUsuario(Usuario usuario) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(usuario.getLogin());
		if (usuarioNoBanco != null) {
			usuarioNoBanco = usuarioDAO.atualizar(usuario);
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public List<Categoria> getCategorias(int idUsuario) {
		return usuarioDAO.getCategorias(idUsuario);
	}

	public RespostaServico atualizarCategorias(int idUsuario, List<Categoria> categoriasDoUsuario) {
		categoriasDoUsuario.forEach((c) -> categoriaDAO.salvar(c));
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		if (usuarioNoBanco != null) {
			usuarioNoBanco.setCategorias(categoriasDoUsuario);
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;

	}

	public RespostaServico adicionarCategoria(int idUsuario, Categoria categoria) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		if (usuarioNoBanco != null) {
			categoriaDAO.salvar(categoria);
			usuarioNoBanco.getCategorias().add(categoria);
			resposta.setResultadoServico(SUCESSO);
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico removerCategoria(int idUsuario, Categoria categoria) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		if (usuarioNoBanco != null) {
			// verifica se o usuario possui a categoria
			Optional<Categoria> categoriaOp = usuarioNoBanco.getCategorias().stream()
					.filter((x) -> x.getNome().equals(categoria.getNome())).findAny();
			if (categoriaOp.isPresent()) {
				categoriaDAO.remover(categoriaOp.get());
				usuarioNoBanco.getCategorias().remove(categoriaOp.get());
				resposta.setResultadoServico(SUCESSO);
			} else {
				resposta.setResultadoServico(CATEGORIA_NAO_ENCONTRADA);
			}
		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

	public RespostaServico removerTodasCategoria(int idUsuario) {
		Usuario usuarioNoBanco = usuarioDAO.buscarUsuario(idUsuario);
		if (usuarioNoBanco != null) {
			// deleta cada categoria que o usuario possui
			usuarioNoBanco.getCategorias().clear();
			resposta.setResultadoServico(SUCESSO);

		} else {
			resposta.setResultadoServico(USUARIO_NAO_ENCONTRADO);
		}
		resposta.setResposta(usuarioNoBanco);
		return resposta;
	}

}
