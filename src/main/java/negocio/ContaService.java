package negocio;

import java.util.List;

import javax.ejb.Stateless;

import dominio.Conta;
import dominio.Usuario;
import util.ApiServicoWeb;

@Stateless
public class ContaService {	
	private ApiServicoWeb api;
	
	public ContaService() {
		api = new ApiServicoWeb();
	}
	
	public void cadastrar(Usuario usuario, Conta conta) {
		//conta.getUsuario().getContas().add(conta);
		api.adicionarConta(usuario, conta);
	}

	/**
	 * atualiza as contas de dado usuario
	 * @param conta determinada conta
	 */
	public void atualizar(Usuario usuario, Conta conta) {
		/*
		List<Conta> contas = conta.getUsuario().getContas();
		
		// encontra o index da conta
		int index = 0;
		for (Iterator<Conta> it = contas.iterator(); it.hasNext(); it.next()) {
			if (it.next().getIdConta() == conta.getIdConta())
				break;
			index++;
		}
		contas.set(index, contaDAO.atualizar(conta)); */
		api.editarConta(usuario, conta);
		//contaDAO.atualizar(conta);
	}

	public void deletarConta(Usuario usuario, Conta conta) {
		//conta.getUsuario().getContas().remove(conta);
		api.deletarConta(usuario, conta);
		//contaDAO.remover(conta);
	}
	
	public List<Conta> getContasUsuario(Usuario usuario) {
		return api.getContasUsuario(usuario);
	}
	
	/*
	public List<Conta> getContasUsuario(Usuario usuario) {
		return contaDAO.buscarContaUsuario(usuario);
	}
	
	public List<Conta> getContasUsuarioId(int idUsuario) {
		return contaDAO.buscarContaUsuario(idUsuario);
	}*/
}
