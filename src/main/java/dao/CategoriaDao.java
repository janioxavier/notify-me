package dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import dominio.Categoria;

@Stateless
public class CategoriaDao {
	@PersistenceContext
	private EntityManager em;

	public Categoria salvar(Categoria c) {
		em.persist(c);		
		return c;
	}

	public Categoria atualizar(Categoria c) {
		Categoria categoria = em.merge(c);
		c = categoria;
		return c;
	}

	public Categoria remover(Categoria c) {
		c = em.find(Categoria.class, c.getIdCategoria());
		em.remove(c);
		return c;
	}
}
