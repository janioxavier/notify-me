package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Categoria;
import dominio.Usuario;

@Stateless
public class UsuarioDao {
	@PersistenceContext
	private EntityManager em;
	
	public Usuario buscarUsuario(String login) {
		String qs = "from Usuario c where c.login = :login";
		Query q = em.createQuery(qs);
		q.setParameter("login", login);
		try {
			Usuario usuario = (Usuario) q.getSingleResult();
			usuario.setCategorias(getCategorias(usuario.getIdUsuario()));
			return usuario;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public void salvar(Usuario user){
		em.persist(user);
	}
	
	public Usuario atualizar(Usuario user) {
		Usuario u = em.merge(user);
		user = u;
		return user;
	}

	public void deletar(Usuario user) {
		user = em.find(Usuario.class, user.getIdUsuario());
		em.remove(user);
	}

	public Usuario buscarUsuario(int id) {
		// TODO Auto-generated method stub
		Usuario usuario = em.find(Usuario.class, id);
		usuario.setCategorias(getCategorias(id));
		return usuario;
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarios() {
		String qs = "select u from Usuario u";
		Query q = em.createQuery(qs);
		return (List<Usuario>) q.getResultList();
	}

	public boolean temUsuario(String login) {
		return buscarUsuario(login) != null ? true : false;
	}

	public boolean possuiCategoria(int idUsuario, Categoria categoria) {
		return getCategorias(idUsuario).contains(categoria);
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> getCategorias(int idUsuario) {
		String qs = "select u.categorias FROM Usuario u where u.idUsuario = :idUsuario";
		Query q = em.createQuery(qs);
		q.setParameter("idUsuario", idUsuario);
		return (List<Categoria>)  q.getResultList();
	}
}
