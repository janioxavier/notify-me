package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Conta;

@Stateless
public class ContaDao {
	@PersistenceContext
	private EntityManager em;

	public void salvar(Conta c) {
		em.persist(c);
	}

	public void atualizar(Conta c) {
		Conta conta = em.merge(c);
		c = conta;
		//return null;
	}

	public void remover(Conta c) {
		c = em.find(Conta.class, c.getIdConta());
		em.remove(c);
	}
	
	public Conta buscarConta(int id) {
		Conta c = em.find(Conta.class, id);
		return c;
	}

	@SuppressWarnings("unchecked")
	public List<Conta> listar() {
		String qs = "select c from Conta c";
		Query q = em.createQuery(qs);
		return (List<Conta>) q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Conta> buscarContaNome(String nome) {
		String qs = 
				"from Conta c where c.name = :nome";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (List<Conta>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param usuario usuario cadastrado no sistmea
	 * @return todas as contas de determinado usuario
	 *
	public List<Conta> buscarContaUsuario(Usuario usuario) {
		String qs = 
				"from Conta c where c.usuario = :usuario";
		Query q = em.createQuery(qs);
		q.setParameter("usuario", usuario);
		return (List<Conta>) q.getResultList();	
	}*/
	
	
	/**
	 * 
	 * @param usuario usuario cadastrado no sistmea
	 * @return todas as contas de determinado usuario
	 *
	public List<Conta> buscarContaUsuario(int idUsuario) {
		String qs = 
				"from Conta c where c.usuario.idUsuario = :usuario";
		Query q = em.createQuery(qs);
		q.setParameter("usuario", idUsuario);
		return (List<Conta>) q.getResultList();	
	}*/

	public boolean remover(int id) {
		Conta conta = em.find(Conta.class, id);		
		if (conta != null) {
			em.remove(conta);
			return true;
		}
		else {
			return false;
		}			
	}
}
