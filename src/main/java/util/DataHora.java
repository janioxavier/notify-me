package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DataHora {

	public static Date getDataHoraAtual() {
		return Date.from(Instant.now());
	}

	public static Date addDia(Date date, int dia) {
		Instant instant = date.toInstant().plus(dia, ChronoUnit.DAYS);
		return Date.from(instant);
	}

	public static Date diminuirDia(Date date, int dia) {
		Instant instant = date.toInstant().minus(dia, ChronoUnit.DAYS);
		return Date.from(instant);
	}

	public static Date addHoraMinuto(Date date, Date horaMinuto) {
		Instant instant = date.toInstant().plus(getHora(horaMinuto), ChronoUnit.HOURS)
				.plus(getMinuto(horaMinuto),ChronoUnit.MINUTES);
		
		return Date.from(instant);
	}

	public static Date addHoraMinuto(Date date, int hora, int minuto) {
		Instant instant = date.toInstant().plus(hora, ChronoUnit.HOURS).plus(minuto, ChronoUnit.MINUTES);
		return Date.from(instant);
	}

	public static boolean ehMesmoDiaHoraMinuto(Date data1, Date data2) {
		return ehMesmoDia(data1, data2);
	}

	public static boolean ehMesmoHoraMinuto(Date data1, Date data2) {
		return formatarHoraMinuto(data1).equals(formatarHoraMinuto(data2));
	}

	public static boolean ehMesmoDia(Date dia1, Date dia2) {
		return formatarData(dia1).equals(formatarData(dia2));
	}

	public static String formatarData(Date date) {
		return DateFormat.getDateInstance(DateFormat.SHORT).format(date);
	}

	public static String formatarHoraMinuto(Date date) {
		return DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
	}

	public static long getHora(Date date) {
		return Long.parseLong(formatarHoraMinuto(date).split(":")[0]);
	}

	public static long getMinuto(Date date) {
		return Long.parseLong(formatarHoraMinuto(date).split(":")[1]);
	}
	
	public static Date getHoraMinuto(String string) {
		// TODO Auto-generated method stub
		try {
			return DateFormat.getTimeInstance(DateFormat.SHORT).parse(string);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String... strings) {
		java.util.Date date = Date.from(Instant.now());
		System.out.println("Hora minuto: " + getHoraMinuto("12:15"));

		// System.out.println(date.toInstant().minus(2,
		// ChronoUnit.DAYS).parse("dd/MM/uuuu"));
		// System.out.println(ehMesmoDia(date,
		// date.from(Instant.now().plusSeconds(86400))));
		System.out.println(addHoraMinuto(date, 2, 2));
		System.out.println(getHora(date));
		System.out.println(getMinuto(date));

		try {
			Date hoje = DateFormat.getDateInstance(DateFormat.SHORT).parse(formatarData(date));
			System.out.println("Data hoje: " + hoje);
			Date horaMinuto = DateFormat.getTimeInstance(DateFormat.SHORT).parse(formatarHoraMinuto(date));
			System.out.println("Hora Minuto data: " + horaMinuto);

			System.out.println("Hora Minuto adicionado: " + addHoraMinuto(hoje, horaMinuto));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(LocalDate.now(date.toInstant().parse("uuuu/MM/dd")));
	}

	
}
