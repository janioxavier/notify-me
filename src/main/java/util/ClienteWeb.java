package util;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;

import javax.ws.rs.core.MediaType;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import dominio.Conta;

public class ClienteWeb {

	public ClienteWeb() {
		
	}

	public int doDelete(String url) throws URISyntaxException, ClientProtocolException, IOException {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpDelete request = new HttpDelete(new URL(url).toURI());
		
		return client.execute(request).getStatusLine().getStatusCode();
	}

	public InputStream doPut(String urlString, String content) throws URISyntaxException, ClientProtocolException, IOException {
		DefaultHttpClient client = new DefaultHttpClient();
		URL url = new URL(urlString);
		HttpPut request = new HttpPut(url.toURI());
		StringEntity entity = new StringEntity(content);
		entity.setContentType(MediaType.APPLICATION_XML);
		request.setEntity(entity);
		return client.execute(request).getEntity().getContent();
	}

	public InputStream doPost(String url, String content) throws URISyntaxException, ClientProtocolException, IOException {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(new URL(url).toURI());
		//Form form = new Form();		

		StringEntity input = new StringEntity(content);
		input.setContentType(MediaType.APPLICATION_XML);
		request.setEntity(input);

		return client.execute(request).getEntity().getContent();
	}

	public InputStream doGet(String urlString) throws URISyntaxException, ClientProtocolException, IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.disconnect();
		return conn.getInputStream();
	}

	public static void main(String... args) {
		XMLDecoder d = null;				
		try {
			d = new XMLDecoder(new BufferedInputStream(new FileInputStream("Test.xml")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (d != null)
				d.close();
		}
		Conta result = (Conta) d.readObject();
		System.out.println(result);
	}
}
