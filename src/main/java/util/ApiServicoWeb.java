package util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.client.ClientProtocolException;

import dominio.Conta;
import dominio.RespostaServico;
import dominio.ResultadoServico;
import dominio.Usuario;

public class ApiServicoWeb {
	private ClienteWeb client;
	private final String URL = "http://localhost:8080/NotifyMe/api/UsuarioService";
	private JAXBContext context;
	private RespostaServico resposta;

	public ApiServicoWeb() {
		client = new ClienteWeb();
	}

	public RespostaServico doLogin(String login, String senha) {
		String url = URL + "/user;login=" + login + ";senha=" + senha;
		try {
			context = JAXBContext.newInstance(RespostaServico.class);
			Unmarshaller um = context.createUnmarshaller();
			resposta = (RespostaServico) um.unmarshal(client.doGet(url));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resposta;
	}

	public List<Conta> getContasUsuario(Usuario usuario) {
		String url = URL + "/user/" + usuario.getIdUsuario();
		try {
			context = JAXBContext.newInstance(RespostaServico.class);
			Unmarshaller um = context.createUnmarshaller();
			resposta = (RespostaServico) um.unmarshal(client.doGet(url));
			switch (resposta.getResultadoServico()) {
			case SUCESSO:
				return resposta.getResposta().getContas();
			default:
				break;
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Usuario adicionarContaUsuario(Usuario usuario) {
		StringWriter writer = new StringWriter();
		String url = URL + "/user";
		try {
			context = JAXBContext.newInstance(Usuario.class);
			Marshaller m = context.createMarshaller();

			m.marshal(usuario, writer);
						
			context = JAXBContext.newInstance(RespostaServico.class);
			Unmarshaller um = context.createUnmarshaller();
			
			InputStream is = client.doPut(url, writer.toString());
			
			resposta = (RespostaServico) um.unmarshal(is); 

			return resposta.getResposta();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public Usuario adicionarConta(Usuario usuario, Conta conta) {
		StringWriter writer = new StringWriter();
		String url = URL + "/user/" + usuario.getIdUsuario() + "/conta";
		try {
			context = JAXBContext.newInstance(Conta.class);
			Marshaller m = context.createMarshaller();

			m.marshal(conta, writer);
						
			context = JAXBContext.newInstance(RespostaServico.class);
			Unmarshaller um = context.createUnmarshaller();
			
			InputStream is = client.doPut(url, writer.toString());
			
			resposta = (RespostaServico) um.unmarshal(is); 

			return resposta.getResposta();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public RespostaServico editarConta(Usuario usuario, Conta conta) {
		StringWriter writer = new StringWriter();
		String url = URL + "/user/" + usuario.getIdUsuario() + "/conta";

		try {
			context = JAXBContext.newInstance(Conta.class);
			Marshaller m = context.createMarshaller();

			m.marshal(conta, writer);

			context = JAXBContext.newInstance(RespostaServico.class);
			Unmarshaller un = context.createUnmarshaller();
			InputStream is = client.doPost(url, writer.toString());

			return ((RespostaServico) un.unmarshal(is));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public RespostaServico deletarConta(Usuario usuario, Conta conta) {
		String url = URL + "/user/" + usuario.getIdUsuario() + "/conta/" + conta.getIdConta();

		try {
			if (usuario != null) {
				usuario.getContas().remove(conta);
				client.doDelete(url);
				resposta.setResultadoServico(ResultadoServico.SUCESSO);
			} else {
				resposta.setResultadoServico(ResultadoServico.USUARIO_NAO_ENCONTRADO);
			}			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resposta;
	}
	
	
	public RespostaServico deletarContaUsuario(Usuario usuario) {
		String url = URL + "/user/" + usuario.getIdUsuario();
		try {			
			client.doDelete(url);
			resposta.setResultadoServico(ResultadoServico.SUCESSO);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resposta;
	}

	public static void main(String... args) {
		ApiServicoWeb api = new ApiServicoWeb();
		// Scanner r = new Scanner(System.in);
		Usuario usuarioNovo = new Usuario();
		// cadastrar usuario
		usuarioNovo.setLogin("usario_api");
		usuarioNovo.setSenha("123");
		usuarioNovo = api.adicionarContaUsuario(usuarioNovo);		

		// realizar login
		Usuario usuario = api.doLogin("usario_api", "123").getResposta();
		System.out.println(usuario);

		// cadastrar conta
		Conta conta = new Conta();
		conta.setNome("conta criada na api");
		conta.setVencimento(DataHora.getDataHoraAtual());
		System.out.println(api.adicionarConta(usuario, conta));

		// editar conta
		conta.setNome("conta editada por meio da api");
		conta.setDescricao("Conta editada por meio da api");
		System.out.println(api.editarConta(usuario, conta));

		// remover conta
		api.deletarConta(usuario, conta);
		System.out.println(api.getContasUsuario(usuario).contains(conta));
	}

}
