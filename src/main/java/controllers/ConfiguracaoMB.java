package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import dominio.Categoria;
import dominio.RespostaServico;
import dominio.ResultadoServico;
import dominio.Usuario;
import view.util.Message;

@ManagedBean(name = "configuracao")
@RequestScoped
public class ConfiguracaoMB {

	@ManagedProperty(value = "#{usuarioMB}")
	private UsuarioMB usuarioMB;

	private Usuario usuario;

	private String novoPassword;

	private String confirmPassword;

	private String novoLogin;

	private Categoria categoria;

	public ConfiguracaoMB() {
		categoria = new Categoria();

	}

	public void setUsuarioMB(UsuarioMB usuarioMB) {
		this.usuarioMB = usuarioMB;
		usuario = usuarioMB.getUsuario();
	}

	public void atualizarSenha() {
		if (confirmPassword.equals(novoPassword)) {
			RespostaServico resposta = usuarioMB.getUsuarioService().atualizarSenha(usuario.getLogin(),
					usuario.getSenha(), novoPassword);
			switch (resposta.getResultadoServico()) {
			case SUCESSO:
				Message.info("senha alterada com sucesso");
				usuario = resposta.getResposta();
				break;
			case SENHA_INCORRETA:
				Message.error("senha incorreta");
				break;
			default:
				break;
			}
		} else {
			Message.error("senha de confirmacao e nova senha devem ser identicos");
		}

		/*
		 * fazer a verificao do novo password e confirm password no jsf
		 * 
		 * int result = usuarioService.atualizarSenha(usuario.getLogin(),
		 * novoPassword, confirmPassword).getResultadoServico() ; antigoPassword
		 * = novoPassword = confirmPassword = null; if (result == 1) { //
		 * sucesso senha alterada com sucesso
		 * Message.info("senha alterada com sucesso"); } else if (result == 0) {
		 * // novo password e antigo não confere
		 * Message.error("novo password e antigo são diferentes"); } else { //
		 * Senha incorreta Message.error("Senha antiga está incorreta"); }
		 */
	}

	public void atualizarProfile() {
		RespostaServico resposta = usuarioMB.getUsuarioService().atualizarProfile(usuario.getLogin(),
				usuario.getFirstName(), usuario.getLastName());
		boolean ehValido = resposta.getResultadoServico() == ResultadoServico.SUCESSO ? true : false;
		if (ehValido) {
			Message.info("Perfil alterado com sucesso");
			usuarioMB.setUsuario(resposta.getResposta());
		} else {
			// firstname ou lastname vazios
			Message.error("Nome e sobrenome devem ser preenchidos");
		}
	}

	public void atualizarLogin() {
		RespostaServico resposta = usuarioMB.getUsuarioService().atualizarLogin(usuario.getLogin(), novoLogin,
				confirmPassword);

		switch (resposta.getResultadoServico()) {
		case SUCESSO:
			Message.info("Login alterado com sucesso");
			usuarioMB.setUsuario(resposta.getResposta());
			break;
		case SENHA_INCORRETA:
			Message.error("password incorreto");
			break;
		case USUARIO_ENCONTRADO:
			Message.error("Login j� utilizado");
			break;
		default:
			break;
		}
	}

	public String deletarConta() {
		RespostaServico resposta = usuarioMB.getUsuarioService().deletarUsuario(usuario.getLogin(), confirmPassword);
		boolean isDeletado = resposta.getResultadoServico() == ResultadoServico.SUCESSO ? true : false;
		if (isDeletado) {
			Message.info("Conta deletada com sucesso");
			usuario = null;
			return usuarioMB.sair();
		} else {
			Message.error("Password incorreto");
		}
		return null;
	}

	public void atualizarUsuario() {
		RespostaServico resposta = usuarioMB.getUsuarioService().atualizarUsuario(usuario);
		boolean isAtualizado = resposta.getResultadoServico() == ResultadoServico.SUCESSO ? true : false;
		if (isAtualizado) {
			Message.info("Configura��o alterada com sucesso");
			// return usuarioMB.sair();
		} else {
			Message.error("Password incorreto");
		}
	}

	public void adicionarCategoria() {
		if (!usuario.getCategorias().contains(categoria)) {
			usuario = usuarioMB.getUsuarioService().adicionarCategoria(usuario.getIdUsuario(), categoria).getResposta();
			usuarioMB.setUsuario(usuario);
		}
	}

	public void removerCategoria() {
		usuario = usuarioMB.getUsuarioService().removerCategoria(usuario.getIdUsuario(), categoria).getResposta();
		usuarioMB.setUsuario(usuario);
	}

	public void removerTodasCategorias() {
		usuario = usuarioMB.getUsuarioService().removerTodasCategoria(usuario.getIdUsuario()).getResposta();
		usuarioMB.setUsuario(usuario);
	}

	public List<String> categoriasQueContem(String nome) {
		List<String> categorias = new ArrayList<String>();
		usuario.getCategorias().forEach((c) -> {
			if (c.getNome().contains(nome))
				categorias.add(c.getNome());
		});
		return categorias;
	}

	public String getNovoPassword() {
		return novoPassword;
	}

	public void setNovoPassword(String novoPassword) {
		this.novoPassword = novoPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getNovoLogin() {
		return novoLogin;
	}

	public void setNovoLogin(String novoLogin) {
		this.novoLogin = novoLogin;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}