package controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.primefaces.context.RequestContext;

import dominio.Categoria;
import dominio.Conta;
import dominio.RespostaServico;
import dominio.ResultadoServico;
import util.DataHora;
import view.util.Message;

@ManagedBean
@RequestScoped
public class ContaMB {

	@ManagedProperty(value = "#{usuarioMB}")
	private UsuarioMB usuarioMB;

	public void setUsuarioMB(UsuarioMB usuarioMB) {
		this.usuarioMB = usuarioMB;
		horaNotificacao = DataHora.getHoraMinuto(usuarioMB.getUsuario().getHoraNotificacaoPadrao());
		idUsuario = usuarioMB.getUsuario().getIdUsuario();
	}

	private int idUsuario;

	private Conta conta;
	private Conta contaAuxiliar;

	private List<Conta> contas;

	private Date periodoInicial;

	private Date periodoFinal;

	private boolean contaAPaga;

	private boolean contasVencidas;

	private Date horaNotificacao;

	private Categoria categoria;

	public ContaMB() {
		conta = new Conta();
		categoria = new Categoria();

		contaAuxiliar = new Conta();
		// usuario = (Usuario) SessionContext.getAttribute("usuarioLogado");
		contaAPaga = true;
		// ((UsuarioMB)
		// FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioMB"))
		// .getUsuario();
		// contas = contaService.getContasUsuario(usuario);
		// listaContas();
	}

	public boolean isContaAPaga() {
		return contaAPaga;
	}

	public void setContaAPaga(boolean ehContaPaga) {
		this.contaAPaga = ehContaPaga;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta Conta) {
		this.conta = Conta;
	}

	public void cadastrar() {
		if (validarConta(conta)) {
			Date vencimentoConfigurado = DataHora.addHoraMinuto(conta.getVencimento(), horaNotificacao);
			conta.setVencimento(vencimentoConfigurado);

			conta.setCategoria(categoria);

			RespostaServico resposta = usuarioMB.getUsuarioService().adicionarConta(idUsuario, conta);
			if (resposta.getResultadoServico() == ResultadoServico.SUCESSO) {
				usuarioMB.setUsuario(resposta.getResposta());
			}

			// usuarioMB.adicionarConta(conta);
			RequestContext.getCurrentInstance().execute("PF('cd').show()");
			/*
			 * Usuario usuario = usuarioMB.getUsuario(); if (usuario.getContas()
			 * == null) { List<Conta> contas = new ArrayList<>();
			 * contas.add(conta); usuarioMB.getUsuario().setContas(contas); }
			 * else { usuarioMB.getUsuario().getContas().add(conta); }
			 * usuarioMB.atualizarUsuario(); //
			 * FacesContext.getCurrentInstance().addMessage("", facesMsg);
			 * RequestContext.getCurrentInstance().execute("PF('cd').show()");
			 * // nextPage = "/home"; conta = new Conta();
			 */
		}
	}

	public void editarConta() {
		if (validarConta(contaAuxiliar)) {
			// contaAuxiliar.setUsuario(usuario);
			RespostaServico resposta = usuarioMB.getUsuarioService().editarConta(idUsuario, contaAuxiliar);
			switch (resposta.getResultadoServico()) {
			case SUCESSO:
				usuarioMB.setUsuario(resposta.getResposta());
				RequestContext.getCurrentInstance().execute("PF('cd').show()");
				break;
			default:
				break;
			}
			// usuarioMB.editarConta(contaAuxiliar);
			// contaService.atualizar(usuarioMB.getUsuario(), contaAuxiliar);
			// FacesContext.getCurrentInstance().addMessage("", facesMsg);

			// nextPage = "/home";
			// contaAuxiliar = new Conta();
		}
	}

	private List<Conta> contasDoUsuario() {
		List<Conta> contasDoUsuario = usuarioMB.getUsuario().getContas();
		usuarioMB.setContasBuscadas(contasDoUsuario);
		return contasDoUsuario;
	}

	public void buscarConta() {
		List<Conta> contas = contasDoUsuario();
		List<Conta> encontradas = new ArrayList<>();
		contas.forEach(c -> {

			if (c.getNome().toLowerCase().contains(conta.getNome().toLowerCase())) {
				encontradas.add(c);
			}

			if (!c.isEstaPaga() && !encontradas.contains(c)) {
				encontradas.add(c);
			}

		});

		// remove das contas encontradas aquelas que nao contem dada categoria
		encontradas.removeIf((c) -> !c.getCategoria().getNome().equals(categoria.getNome()));

		// filtra contas entre o periodo inicial e final
		if (periodoInicial != null && periodoFinal != null) {
			encontradas.removeIf((c) ->
			// remove contas anteriores ao periodo inicial
			c.getVencimento().before(periodoInicial) ||
			// remove contas apos o periodo final
					c.getVencimento().after(periodoFinal));
		}
		// filtra contas at� o periodo final
		else if (periodoInicial == null && periodoFinal != null) {
			encontradas
					// remove as contas apos o periodo final
					.removeIf((c) -> c.getVencimento().after(periodoFinal));
		}
		// filtra contas a partir do periodo inicial
		else if (periodoFinal == null && periodoInicial != null) {
			encontradas.removeIf((c) ->
			// remove contas anteriores ao periodo inicial
			c.getVencimento().before(periodoInicial));
		}

		if (contasVencidas) {
			encontradas.removeIf((c) -> c.getVencimento().before(DataHora.getDataHoraAtual()));
		}

		usuarioMB.setContasBuscadas(encontradas);
	}

	public boolean validarConta(Conta conta) {
		// TODO melhorar a validação
		String msg = null;
		if (conta.getNome() == null) {
			msg = "O nome da conta precisa ser preenchido";
		}
		if (conta.getVencimento() == null) {
			msg += "A conta precisa ter um vencimento\n";
		}
		if (conta.getValor() < 0) {
			msg += "A conta precisa ter um valor positivo\n";
		}

		if (msg != null) {
			System.out.println(msg);
			System.out.println(conta);
			Message.warn("Erro no Cadastro" + "Preencha os campos obrigatórios com valores válidos");
			return false;
		}
		return true;
	}

	public void deletarConta() {
		// contaService.deletarConta(contaAuxiliar);
		RespostaServico resposta = usuarioMB.getUsuarioService().deletarConta(idUsuario, conta.getIdConta());
		switch (resposta.getResultadoServico()) {
		case SUCESSO:
			usuarioMB.setUsuario(resposta.getResposta());
			Message.info("Conta deletada com sucesso");
			break;
		default:
			break;
		}

	}

	/**
	 * lista todas as contas do usuario.
	 * 
	 * @return uma lista com todas as contas do usuario.
	 */
	public List<Conta> listaContas() {
		return contas;
	}

	public Conta getContaAuxiliar() {
		return contaAuxiliar;
	}

	public void setContaAuxiliar(Conta contaAuxiliar) {
		this.contaAuxiliar = contaAuxiliar;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public Date getHoraNotificacao() {
		return horaNotificacao;
	}

	public void setHoraNotificacao(Date horaNotificacao) {
		this.horaNotificacao = horaNotificacao;
	}

	public boolean isContasVencidas() {
		return contasVencidas;
	}

	public void setContasVencidas(boolean contasVencidas) {
		this.contasVencidas = contasVencidas;
	}

	public Date getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public Date getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}
