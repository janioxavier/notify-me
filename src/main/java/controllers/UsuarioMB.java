package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dominio.Categoria;
import dominio.Conta;
import dominio.Usuario;
import negocio.UsuarioService;
import view.util.ContextoWeb;

@ManagedBean(name = "usuarioMB")
@SessionScoped
public class UsuarioMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	@EJB
	private UsuarioService usuarioService;

	private List<Conta> contasBuscadas;

	public UsuarioMB() {
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		if (usuario != null) {
			this.usuario = usuario;
			contasBuscadas = usuario.getContas();
			usuario.setCategorias(usuarioService.getCategorias(usuario.getIdUsuario()));
		}
	}

	public List<String> categoriasQueContem(String nome) {
		List<String> categorias = new ArrayList<String>();
		usuario.getCategorias().forEach((c) -> {
			if (c.getNome().contains(nome))
				categorias.add(c.getNome());
		});
		return categorias;
	}
	
	public void setUsuarioLogado() {
		if (usuario == null) {
			usuario = (Usuario) ContextoWeb.getAttribute("USUARIO_FACE");
			usuario = usuarioService.doLogin(usuario.getLogin(), usuario.getSenha()).getResposta();			
			contasBuscadas = usuario.getContas();
			usuario.setCategorias(usuarioService.getCategorias(usuario.getIdUsuario()));
		}
	}

	public void atualizarUsuario() {
		// usuarioService.atualizarUsuario(usuario);
	}

	public String sair() {
		// SessionContext.terminarSessao();
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public List<Conta> getContasBuscadas() {
		return contasBuscadas;
	}

	public void setContasBuscadas(List<Conta> contasBuscadas) {
		this.contasBuscadas = contasBuscadas;
	}
}
