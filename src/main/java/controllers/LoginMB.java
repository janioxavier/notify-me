package controllers;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.RespostaServico;
import dominio.ResultadoServico;
import dominio.Usuario;
import view.util.ContextoWeb;
import view.util.Message;
import webservice.login.FacebookAuthHelper;

@ManagedBean
@RequestScoped
public class LoginMB {
	@ManagedProperty(value = "#{usuarioMB}")
	private UsuarioMB usuarioMB;

	private Usuario usuario;

	private String login;

	private String senha;
	
	public LoginMB() {
		usuario = new Usuario();
	}

	public void setUsuarioMB(UsuarioMB usuarioMB) {
		this.usuarioMB = usuarioMB;
	}

	public String logar() {
		RespostaServico resposta = usuarioMB.getUsuarioService().doLogin(login, senha);		
		switch (resposta.getResultadoServico()) {
		case SUCESSO:
			usuarioMB.setUsuario(resposta.getResposta());			
			return "/home?faces-redirect=true";
		case SENHA_INCORRETA:
			Message.error("Usuario e/ou senha incorretos");
			break;
		case USUARIO_NAO_ENCONTRADO:
			Message.error("Usuario n�o encontrado");
			break;
		default:
			break;
		}
		return null;
	}

	public String loginFacebook() {				
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect(new FacebookAuthHelper().getLoginRedirectURL());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public String signup() {
		RespostaServico resposta = usuarioMB.getUsuarioService().signup(usuario);
		boolean ehValido = resposta.getResultadoServico() == ResultadoServico.SUCESSO ? true : false;
		if (ehValido) {
			// SessionContext.setAttribute("usuarioLogado",
			// usuarioService.buscarUsuario(usuario.getLogin()));
			usuarioMB.setUsuario(resposta.getResposta());
			return "/home?faces-redirect=true";
		} else {
			Message.error("login já foi utilizado");
			return null;
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
