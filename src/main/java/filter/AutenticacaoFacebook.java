package filter;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dominio.Usuario;
import negocio.UsuarioService;
import webservice.login.FacebookAuthHelper;
import webservice.login.UsuarioFacebook;

@WebFilter("*.facebook")
public class AutenticacaoFacebook implements Filter {

	@EJB
	private UsuarioService usuarioService;

	private static final long serialVersionUID = 8071426090770097330L;

	private FacebookAuthHelper fbHelper;

	public AutenticacaoFacebook() {
		fbHelper = new FacebookAuthHelper();
	}


	/**
	 * converter um usuario do facebook para um usuario da aplica��o
	 * 
	 * @param usuarioFace
	 * @return usuario da aplica��o
	 */
	private Usuario getUsuarioApplication(UsuarioFacebook usuarioFace) {
		Usuario usuario = new Usuario();
		usuario.setFirstName(usuarioFace.getName());
		usuario.setLogin(usuarioFace.getName() + usuarioFace.getId());
		usuario.setSenha("" + usuarioFace.getId());
		return usuario;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		
		String faceCode = request.getParameter("code");

		UsuarioFacebook usuarioFB = fbHelper.obterUsuarioFacebook(faceCode);
		Usuario usuario = getUsuarioApplication(usuarioFB);
		
		HttpSession session = request.getSession();

		try {
			// do some specific user data operation like saving to DB or
			// login user			
			usuarioService.signup(usuario);
			session.setAttribute("USUARIO_FACE", usuario);	
			
			response.sendRedirect(request.getContextPath() + "/home.xhtml");
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(request.getContextPath() + "/login.xhtml");
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}