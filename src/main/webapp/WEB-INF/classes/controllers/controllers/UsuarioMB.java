package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import dao.UsuarioDAO;
import dominio.Usuario;
import view.util.Message;

@ManagedBean
@SessionScoped
public class UsuarioMB {
	private Usuario usuario;
	@Inject
	private UsuarioDAO usuarioDAO;
	
	private String antigoPassword;

	private String novoPassword;

	private String confirmPassword;

	private String novoLogin;

	public UsuarioMB() {
		usuario = new Usuario();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getAntigoPassword() {
		return antigoPassword;
	}

	public void setAntigoPassword(String antigoPassword) {
		this.antigoPassword = antigoPassword;
	}

	public String getNovoPassword() {
		return novoPassword;
	}

	public void setNovoPassword(String novoPassword) {
		this.novoPassword = novoPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getNovoLogin() {
		return novoLogin;
	}

	public void setNovoLogin(String novoLogin) {
		this.novoLogin = novoLogin;
	}

	public String login() {
		Usuario u = usuarioDAO.buscarLogin(usuario.getLogin());
		if (u != null) {
			if (usuario.getLogin().equals(u.getLogin()) && usuario.getSenha().equals(u.getSenha())) {
				usuario = u;
				return "/home.jsf";
			} else {
				Message.error("Usuario e/ou senha incorretos");
				return null;
			}
		} else {
			Message.error("Usuario não encontrado");
			return null;
		}
	}

	public String signup() {
		Usuario u = usuarioDAO.buscarLogin(usuario.getLogin());
		if (u == null) {
			usuarioDAO.salvar(usuario);
			return "/home.jsf";
		} else {
			Message.error("login já foi utilizado");
			return null;
		}
	}

	public void atualizarSenha() {
		// Usuario u = usuarioDAO.buscarLogin(usuario.getLogin());
		if (usuario.getSenha().equals(antigoPassword)) {
			if (novoPassword.equals(confirmPassword)) {
				// sucesso senha alterada com sucesso
				Message.info("senha alterada com sucesso");
				usuario.setSenha(confirmPassword);
				atualizarUsuario();
				antigoPassword = novoPassword = confirmPassword = null;
			} else {
				// novo password e antigo não confere
				Message.error("novo password e antigo são diferentes");
				novoPassword = confirmPassword = null;
			}
		} else {
			// Senha incorreta
			Message.error("Senha antiga está incorreta");
			antigoPassword = novoPassword = confirmPassword = null;
		}
	}

	public void atualizarProfile() {
		if (usuario.getFirstName() != null & usuario.getLastName() != null) {
			if (!usuario.getFirstName().isEmpty() && !usuario.getLastName().isEmpty()) {
				// first name e lastname preenchidos
				Message.info("Perfil alterado com sucesso");
				atualizarUsuario();
			} else {
				// firstname ou lastname vazios
				Message.error("Nome e sobrenome devem ser preenchidos");
			}
		}
	}

	public void atualizarLogin() {
		Usuario u = usuarioDAO.buscarLogin(novoLogin);
		if (u == null) {
			if (usuario.getSenha().equals(confirmPassword)) {
				// nenhum usuario cadastrado com esse login
				Message.info("Login alterado com sucesso");
				usuario.setLogin(novoLogin);
				atualizarUsuario();
				novoLogin = "";
			} else {
				Message.error("password incorreto");
			}
		} else {
			// tem um usuario cadastrado com esse login
			Message.error("Login já utilizado");
		}
	}
	
	public String deletarConta() {
		if (usuario.getSenha().equals(confirmPassword)) {
			confirmPassword = "";
			usuarioDAO.deletar(usuario);
			usuario = null;
			return "/index";
		} else {
			Message.error("Password incorreto");
		}
		return null;
	}

	public void atualizarUsuario() {
		usuarioDAO.atualizar(usuario);
	}
	
	public String sair() {
		usuario = new Usuario();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuarioMB");
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("contaMB");
		return "/index";
	}
}
