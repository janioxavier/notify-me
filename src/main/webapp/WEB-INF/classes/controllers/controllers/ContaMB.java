package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import dao.ContaDAO;
import dominio.Conta;
import dominio.Usuario;
import view.util.Message;

@ManagedBean
@SessionScoped
public class ContaMB {
	private Conta conta;
	private Conta contaAuxiliar;
	@Inject
	private ContaDAO contaDAO;

	private List<Conta> contas;

	private Usuario usuario;

	private String nextPage;

	public ContaMB() {
		conta = new Conta();
		contaAuxiliar = new Conta();
		usuario = ((UsuarioMB) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioMB"))
				.getUsuario();
		// contas = contaDAO.listar();
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta Conta) {
		this.conta = Conta;
	}

	public String cadastrar() {
		nextPage = null;
		if (validarConta(conta)) {
			conta.setUsuario(usuario);
			contaDAO.salvar(conta);
			// FacesContext.getCurrentInstance().addMessage("", facesMsg);
			RequestContext.getCurrentInstance().execute("PF('cd').show()");
			// nextPage = "/home";
			conta = new Conta();
		}
		return nextPage;
	}

	public String editarConta() {
		nextPage = null;
		if (validarConta(contaAuxiliar)) {
			contaAuxiliar.setUsuario(usuario);
			contaDAO.atualizar(contaAuxiliar);
			// FacesContext.getCurrentInstance().addMessage("", facesMsg);
			RequestContext.getCurrentInstance().execute("PF('cd').show()");
			// nextPage = "/home";
			contaAuxiliar = new Conta();
		}
		return nextPage;
	}

	private List<Conta> contasDoUsuario() {
		List<Conta> contas = contaDAO.listar();
		List<Conta> contasDoUsuario = new ArrayList<>();
		contas.forEach(c -> {
			if (c.getUsuario().getIdUsuario() == usuario.getIdUsuario()) {
				contasDoUsuario.add(c);
			}
		});

		return contasDoUsuario;

	}

	public void buscarConta() {
		List<Conta> contas = contasDoUsuario();
		List<Conta> encontradas = new ArrayList<>();
		contas.forEach(c -> {
			if (c.getNome().toLowerCase().contains(conta.getNome().toLowerCase())) {
				encontradas.add(c);
			}
		});
		if (conta.getVencimento() != null) {
			if (contaAuxiliar.getVencimento() != null) {
				setContas(encontradas);
			}
		}
		setContas(encontradas);

	}

	public boolean validarConta(Conta conta) {
		// TODO melhorar a validação
		String msg = null;
		if (conta.getNome() == null) {
			msg = "O nome da conta precisa ser preenchido";
		}
		if (conta.getVencimento() == null) {
			msg += "A conta precisa ter um vencimento\n";
		}
		if (conta.getValor() < 0) {
			msg += "A conta precisa ter um valor positivo\n";
		}

		if (msg != null) {
			System.out.println(msg);
			System.out.println(conta);
			Message.warn("Erro no Cadastro" + 
					"Preencha os campos obrigatórios com valores válidos");
			return false;
		}
		return true;
	}

	public void deletarConta() {
		contaDAO.remover(contaAuxiliar);
		//contas.remove(contaAuxiliar);
		Message.info("Conta deletada com sucesso");
	}

	public List<Conta> listaContas() {
		setContas(contasDoUsuario());
		return contas;
	}

	public Conta getContaAuxiliar() {
		return contaAuxiliar;
	}

	public void setContaAuxiliar(Conta contaAuxiliar) {
		this.contaAuxiliar = contaAuxiliar;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
