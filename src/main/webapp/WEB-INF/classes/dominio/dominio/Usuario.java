package dominio;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idUsuario;
	
	private String firstName;
	
	private String lastName;
	
	@Column(nullable=false)
	private String login;
	
	@Column(nullable=false)
	private String senha;
	
	private boolean antesVencimento;
	private boolean noVencimento;
	private boolean depoisVencimento;
	
	private String numero1;
	private String numero2;
	
	private String email1;
	private String email2;
	private String email3;
	
	private String horaNotificacaoPadrao;
	
	@OneToMany(mappedBy="usuario")
	private List<Conta> contas;

	public Usuario() {
		// Construtor default
		this.horaNotificacaoPadrao = "07:00";
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(int id) {
		this.idUsuario = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isAntesVencimento() {
		return antesVencimento;
	}

	public void setAntesVencimento(boolean antesVencimento) {
		this.antesVencimento = antesVencimento;
	}

	public boolean isNoVencimento() {
		return noVencimento;
	}

	public void setNoVencimento(boolean noVencimento) {
		this.noVencimento = noVencimento;
	}

	public boolean isDepoisVencimento() {
		return depoisVencimento;
	}

	public void setDepoisVencimento(boolean depoisVencimento) {
		this.depoisVencimento = depoisVencimento;
	}

	public String getNumber1() {
		return numero1;
	}

	public void setNumber1(String number1) {
		this.numero1 = number1;
	}

	public String getNumero2() {
		return numero2;
	}

	public void setNumero2(String numero2) {
		this.numero2 = numero2;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getEmail3() {
		return email3;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public String getHoraNotificacaoPadrao() {
		return horaNotificacaoPadrao;
	}

	public void setHoraNotificacaoPadrao(String horaNotificacaoPadrao) {
		this.horaNotificacaoPadrao = horaNotificacaoPadrao;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNumero1() {
		return numero1;
	}

	public void setNumero1(String numero1) {
		this.numero1 = numero1;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + idUsuario + ", firstName=" + firstName + ", lastName=" + lastName + ", login=" + login
				+ ", senha=" + senha + ", antesVencimento=" + antesVencimento + ", noVencimento=" + noVencimento
				+ ", depoisVencimento=" + depoisVencimento + ", numero1=" + numero1 + ", numero2=" + numero2
				+ ", email1=" + email1 + ", email2=" + email2 + ", email3=" + email3 + ", horaNotificacaoPadrao="
				+ horaNotificacaoPadrao + ", contas=" + contas + "]";
	}


}
