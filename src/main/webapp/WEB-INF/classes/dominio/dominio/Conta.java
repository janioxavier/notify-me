package dominio;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "conta")
public class Conta {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idConta;

	@Column(nullable = false)
	private String nome;
	private String descricao;
	@Temporal(value = TemporalType.DATE)
	private Date vencimento;
	@Column(nullable = false)
	private double valor;
	private String caminhoImagem;

	private String horaNotificacao;

	@ManyToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;

	public Conta() {
		// Construtor default
	}

	public int getIdConta() {
		return idConta;
	}

	public void setIdConta(int id) {
		this.idConta = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getCaminhoImagem() {
		return caminhoImagem;
	}

	public void setCaminhoImagem(String caminhoImagem) {
		this.caminhoImagem = caminhoImagem;
	}

	public String getHoraNotificacao() {
		return horaNotificacao;
	}

	public void setHoraNotificacao(String horaNotificacao) {
		this.horaNotificacao = horaNotificacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Conta [id=" + idConta + ", nome=" + nome + ", descricao=" + descricao + ", vencimento=" + vencimento
				+ ", valor=" + valor + ", caminhoImagem=" + caminhoImagem + ", horaNotificacao=" + horaNotificacao
				+ ", usuario=" + usuario + "]";
	}
}
