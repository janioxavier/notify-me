package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Conta;

@Stateless
public class ContaDAO {
	@PersistenceContext
	private EntityManager em;

	public void salvar(Conta c) {
		em.persist(c);
	}

	public void atualizar(Conta c) {
		Conta conta = em.merge(c);
		c = conta;
	}

	public void remover(Conta c) {
		c = em.find(Conta.class, c.getIdConta());
		em.remove(c);
	}

	@SuppressWarnings("unchecked")
	public List<Conta> listar() {
		String qs = "select c from Conta c";
		Query q = em.createQuery(qs);
		return (List<Conta>) q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Conta> buscarContaNome(String nome) {
		String qs = 
				"from Conta c where c.name = :nome";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (List<Conta>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}
