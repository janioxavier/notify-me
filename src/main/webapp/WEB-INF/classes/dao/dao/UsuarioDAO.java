package dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Conta;
import dominio.Usuario;

@Stateless
public class UsuarioDAO {
	@PersistenceContext
	private EntityManager em;
	
	public Usuario buscarLogin(String login) {
		String qs = "from Usuario c where c.login = :login";
		Query q = em.createQuery(qs);
		q.setParameter("login", login);
		try {
			return (Usuario) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public void salvar(Usuario user){
		em.persist(user);
	}
	
	public void atualizar(Usuario user) {
		Usuario u = em.merge(user);
		u = user;
	}

	public void deletar(Usuario user) {
		user = em.find(Usuario.class, user.getIdUsuario());
		em.remove(user);
	}
}
