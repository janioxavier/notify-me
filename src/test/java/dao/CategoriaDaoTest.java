package dao;

import static org.junit.Assert.fail;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.junit.Before;
import org.junit.Test;

public class CategoriaDaoTest {

	@Before
	public void setUp() throws Exception {

		final Properties jndiProperties = new Properties();
		
		jndiProperties.put(Context.PROVIDER_URL,"remote://localhost:8080/NotifyMe");
		jndiProperties.put(Context.SECURITY_PRINCIPAL,"admin");
		jndiProperties.put(Context.SECURITY_CREDENTIALS,"admin");
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY,"org.jboss.naming.remote.client.InitialContextFactory");
		jndiProperties.put("remote.connectionprovider","org.xnio.XnioProvider");
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");    
		jndiProperties.put("remote.connection.one.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS",false);

		final Context context = new InitialContext(jndiProperties);
		
		final String lookupName = "java:global/NotifyMe/CategoriaDao!dao.CategoriaDao";
		
		CategoriaDao categoriaDAO = (CategoriaDao) context.lookup(lookupName);
	}

	@Test
	public void testSalvar() {
		fail("Not yet implemented");
	}

	@Test
	public void testAtualizar() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemover() {
		fail("Not yet implemented");
	}

}
